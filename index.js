// DOM document object model

// querySelector function takes a string input that is formatted like CSS selector when applying styles. this allows to get a specific element. 
// we can contain a code inside a constant


// alternative
// document.getElementById('#txt-first-name');
// document.getElementsByClassName();
// document.getElementsByTagName();

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name')	
const spanFullName = document.querySelector('#span-full-name');

let fullName = () => {
	return spanFullName.innerHTML = txtFirstName.value + " " +txtLastName.value
}
	// addEventListener has two strings (keyup and event)
	txtFirstName.addEventListener('keyup', fullName);
	txtLastName.addEventListener('keyup', fullName);

	// txtLastName.addEventListener('keyup', (fullName) => {
	// 	spanFullName.innerHTML = txtLastName.value
	// })



	// txtFirstName.addEventListener('keyup', (e) => {
	// 	console.log(e.target);
	// 	console.log(e.target.value);
	// })



